var User = require("./user");
var lunchCalc = require("./lunch");
var exphbs = require('express-handlebars');
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var mongoose = require("mongoose");
var maps = require("./maps");
var old = require("./old");
mongoose.connect("mongodb://db:db@ds039073.mongolab.com:39073/school-schedule");
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(callback) {
    console.log("connected");
});
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport();
var app = require('express')();
var server = require('http').Server(app);

server.listen(process.env.PORT || 6666);

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());
app.use(cookieParser());
app.engine("handlebars", exphbs({
    defaultLayout: "main",
}));
app.use("/favicon", require("./favicon"));
app.use("/old",old);
app.set("view engine", "handlebars");
app.get("/", function(req, res) {
    if (req.cookies.user !== null) {
        User.findById(req.cookies.user, function(err, data) {
            if (err) throw err;
            if (data === null) {
                renderPage(res, null, null, false);
            } else {
                renderPage(res, data, null, true);
            }

        });

    } else {
        renderPage(res, null, null, false);
    }
});
app.get("/dev", function(req, res) {
    if (req.cookies.user !== null) {
        User.findById(req.cookies.user, function(err, data) {
            if (err) throw err;
            if (data === null) {
                renderPageDev(res, null, null, false);
            } else {
                renderPageDev(res, data, null, true);
            }

        });

    } else {
        renderPageDev(res, null, null, false);
    }
});
app.get("/new", function(req, res) {
    res.sendFile(__dirname + "/new.html");
});
app.get('/schedule.min.js', function(req, res) {
    res.sendFile(__dirname + '/schedule.min.js');
});
app.get('/schedule.js', function(req, res) {
    res.sendFile(__dirname + '/schedule.js');
});

app.get('/index.css', function(req, res) {
    res.sendFile(__dirname + '/main.css');
});
app.get('/loading.gif', function(req, res) {
    res.sendFile(__dirname + '/loading.gif');
});
app.get("/suggest", function(req, res) {
    res.sendFile(__dirname + '/suggestions.html');
});
app.get("/suggestions.css", function(req, res) {
    res.sendFile(__dirname + "/suggestions.css");
});
app.post("/suggest", function(req, res) {
    transporter.sendMail({
        from: req.body.name.replace(" ", "") + "@rlclock.tk",
        to: 'gilbert.rosenthal@roxburylatin.org',
        subject: 'New Suggestion for rlclock',
        text: req.body.suggestion + "\nfrom " + req.cookies.user
    });
    transporter.sendMail({
        from: req.body.name.replace(" ", "") + "@rlclock.tk",
        to: 'jonathan.weiss@roxburylatin.org',
        subject: 'New Suggestion for rlclock',
        text: req.body.suggestion + "\nfrom " + req.cookies.user
    });
    if (req.cookies.user !== null) {
        User.findById(req.cookies.user, function(err, data) {
            if (err) throw err;
            if (data === null) {
                renderPage(res, null, "Suggestion successfully sent", false);
            } else {
                console.log(data);
                renderPage(res, data, "Suggestion successfully sent", true);
            }
        });

    } else {
        renderPage(res, null, false);
    }
});

app.get("/tapir",function(req, res) {
   res.sendFile(__dirname+"/tapir.jpg"); 
});

app.post("/new", function(req, res) {
    console.log(req.body);
    var a = {
        name: req.body.a,
        lunch: req.body.alunch
    };
    var b = {
        name: req.body.b,
        lunch: req.body.blunch
    };
    var c = {
        name: req.body.c,
        lunch: req.body.clunch
    };
    var d = {
        name: req.body.d,
        lunch: req.body.dlunch
    };
    var e = {
        name: req.body.e,
        lunch: req.body.elunch
    };
    var f = {
        name: req.body.f,
        lunch: req.body.flunch
    };
    var g = {
        name: req.body.g,
        lunch: req.body.glunch
    };
    var h = {
        name: req.body.h,
        lunch: req.body.hlunch
    };
    generateID(function(num) {
        var newUser = new User({
            _id: num,
            a: a,
            b: b,
            c: c,
            d: d,
            e: e,
            f: f,
            g: g,
            h: h,
            audio: false,
            notifications: true
        });
        newUser.save();
        res.setHeader("Set-Cookie", "user=" + num);
        res.setHeader("XRedirect", "signup");
        res.setHeader("XSuccess", "true");
        res.setHeader("XID", num);
        res.redirect("http://rlclock.tk/");
    });
});

app.get("/login", function(req, res) {
    res.sendFile(__dirname + "/login.html");
});

app.post("/login", function(req, res) {
    User.findById(req.body.id, function(err, data) {
        if (err) throw err;
        if (data !== null) {
            res.setHeader("Set-Cookie", "user=" + req.body.id);
            res.setHeader("XRedirect", "login");
            res.setHeader("XSuccess", "true");
            res.redirect("http://rlclock.tk/");
        } else {
            res.setHeader("XRedirect", "login");
            res.setHeader("XSuccess", "false");
        }
    });
});

app.post("/settings", function(req, res) {
    console.log(req.body);
    User.findById(req.body.user, function(err, data) {
        if (err) throw err;
        var newu = new User(data);
        newu.notifications = req.body.notifications;
        newu.audio = req.body.audio;
        newu.save();
    });
    res.status(200).send("ok");
});

var getDay = function(day) {
    if (maps.days[day] && maps.days[day].indexOf("Drop") !== -1) {
        return maps.days[day].replace("Drop", "").trim();
    } else {
        return maps.days[day] || 0; // Jon: return "0" (no school) if no such key
    }
};

var isDrop = function(day) {
    if (maps.days[day] && maps.days[day].indexOf("Drop") !== -1) {
        return true;
    } else {
        return false;
    }
};
var getLunch = function(day) {
    return maps.lunch[day] || 0;
};

var generateID = function(callback) {
    var random = Math.round(Math.random() * 10000);
    console.log(random);
    User.findById(random, function(err, data) {
        if (err) callback(random);
        else if (data) {
            callback(generateID());
        } else {
            callback(random);
        }
    });
};
var renderPage = function(res, data, notification, auth) {
    //var day = JSON.parse(get("http://casper.roxburylatin.org/daytype.json"));
    //if (day.hallLength) {
        //day.dayType += "-" + day.hallLength;
    //}
    if (auth) {
        getLunchNumber(data._id, getDay(new Date().toDateString()), function(num) {
            //console.log(day);
            res.render("main", {
                day: "E",
                lunch: getLunch(new Date().toDateString()),
                drop: isDrop(new Date().toDateString()),
                auth: true,
                notification: notification,
                a: data.a.name,
                b: data.b.name,
                c: data.c.name,
                d: data.d.name,
                e: data.e.name,
                f: data.f.name,
                g: data.g.name,
                h: data.h.name,
                id: data._id,
                lunchblock: num
            });
        });
    } else {
        res.render("main", {
            day: "E",
            lunch: getLunch(new Date().toDateString()),
            drop: isDrop(new Date().toDateString()),
            auth: false,
            notification: notification
        });
    }
};
var renderPageDev = function(res, data, notification, auth) {
    if (auth) {
        res.render("dev", {
            day: getDay(new Date().toDateString()),
            lunch: getLunch(new Date().toDateString()),
            drop: isDrop(new Date().toDateString()),
            auth: true,
            notification: notification,
            a: data.a.name,
            b: data.b.name,
            c: data.c.name,
            d: data.d.name,
            e: data.e.name,
            f: data.f.name,
            g: data.g.name,
            h: data.h,
            id: data._id
        });
    } else {
        res.render("dev", {
            day: getDay(new Date().toDateString()),
            lunch: getLunch(new Date().toDateString()),
            auth: false,
            notification: notification
        });
    }
};
var getLunchNumber = function(id, day, cb) {
    User.findById(id, function(err, data) {
        var toReturn;
        if (err) throw err;
        var block = lunchCalc.getLunchBlock(day);
        // Jon: why not toReturn = data[block.toLowerCase()].lunch; instead of that nasty switch thing?
        switch (block) {
            case "A":
                toReturn = data.a.lunch;
            case "B":
                toReturn = data.b.lunch;
            case "C":
                toReturn = data.c.lunch;
            case "D":
                toReturn = data.d.lunch;
            case "E":
                toReturn = data.e.lunch;
            case "F":
                toReturn = data.f.lunch;
            case "G":
                toReturn = data.g.lunch;
            case "H":
                toReturn = data.h.lunch;
        }
        console.log(toReturn);
        cb(toReturn);
    });
};

function get(url) {
    var request = require('sync-request');
    var res = request('GET', url);
    console.log(res.getBody().toString());
    return res.getBody().toString();
}