/*if (!Array.isArray) {
  Array.isArray = function(arg) {
    return Object.prototype.toString.call(arg) === "[object Array]";
  };
}*/

if (!Date.now) {
  Date.now = function() {
    return new Date().getTime();
  };
}

(function() {
  "use strict";
 
  var blockLetters = "ABCDEFGH", currentBlock = 0, fullMinutes, homeroomStart = 495, isDrop, minutesLeft, oldBlock, today;
  var days = [];
  
  var dom = {
    periodsList: document.getElementById("periods-list"),
    currentBlock: document.getElementById("current-block"),
    minutesLeft: document.getElementById("minutes-left"),
    settingsButton: document.getElementById("settings-button"),
    settings: document.getElementById("settings"),
    settingsSubmit: document.getElementById("settings-submit"),
    dayType: document.getElementById("day-type"),
    classes: document.getElementById("classes"),
    lunch: document.getElementById("lunch"),
    lunchBlock: document.getElementById("lunchblock"),
    account: {
      nav: document.getElementById("account-nav"),
      on: document.getElementById("account-on"),
      off: document.getElementById("account-off")
    },
    nav: {
      button: document.getElementById("nav-button"),
      el: document.getElementsByTagName("nav")[0]
    }
  };
  
  var lastNotification;
  
  var User = {
    loggedin: false,
    classes: {},
    setClassesFrom: function(dataEl) {
      var children = dataEl.childNodes;
      for (var i = 0; i < children.length; i++) {
        if (children[i].nodeType === window.Node.ELEMENT_NODE) { // if node is element node, it has data
          this.classes[children[i].id.toUpperCase()] = children[i].textContent; // e.g. userData["A"] = "Free"
        }
      }
    }
  };
  
  var now = new Date();
  
  if (document.getElementById("loggedin").textContent === "true") {
    User.loggedin = true;
  }
  fixAccountNav();
  
  function fixAccountNav() {
    if (User.loggedin) {
      dom.account.on.style.display = "block"; // logged in, show sign out button
      dom.account.off.style.display = "none";
    } else {
      dom.account.off.style.display = "block"; // logged out, show log in buttons
      dom.account.on.style.display = "none";
    }
    dom.account.nav.style.display = "block";
  }

  function Block(letter, start, length, name) {
    this.letter = letter;
    if (name) {
      this.name = name;
    }
    this.start = start;
    this.end = start + length;
    this.length = length;
  }
  
  Block.prototype.text = function() {
    return this.name ? this.letter + ": " + this.name : this.letter;
  };
  
  Block.prototype.domElement = function() {
    var child;
    this.element = document.createElement("div");
    this.element.className = "list-item";
    child = document.createElement("div");
    child.className = "list-item-letter";
    child.textContent = this.text();
    this.element.appendChild(child);
    child = document.createElement("div");
    child.className = "list-item-time";
    child.textContent = Block.minutesToString(this.start) + "-" + Block.minutesToString(this.end);
    this.element.appendChild(child);
    return this.element;
  };

  Block.minutesToString = function(mins) {
    var h = (Math.floor(mins / 60) % 12 || 12).toString();
    var m = (mins % 60).toString();
    return h + ":" + (m.length === 1 ? "0" + m : m);
  };

  function Day(dayStr, isDrop) {
    this.dayString = dayStr;
    if (this.dayString === "No Classes" || this.dayString === "Weekend") { // no school, no need to do anything else
      this.noClasses = true;
      return this;
    }
    var splitDay = dayStr.split("-");
    if (splitDay[1] === "S") { // special schedule, again, no need
      this.special = true;
      return this;
    }
    this.dayLetter = splitDay[0];
    this.hallMinutes = 0;
    if (splitDay.length > 1) {
      this.hallMinutes = parseInt(splitDay[1], 10);
    }

    this.periodDuration = 45;
    if (this.hallMinutes > 30) {
      if (this.hallMinutes === 45) {
        this.periodDuration = 44;
      } else if (this.hallMinutes === 60) {
        this.periodDuration = 42;
      } else if (this.hallMinutes === 70) {
        this.periodDuration = 40;
      }
    }
    this.isDrop = isDrop;
    this.periodsLength = 7 - this.isDrop;
    this.lunchPeriod = this.hallMinutes >= 45 ? 2 : 3;

    this.blocks = [];
    var lastBlock;
    this.blocks.push(new Block("Homeroom", homeroomStart, 5));
    if (this.hallMinutes) {
      this.blocks.push(new Block(this.hallMinutes + " minute hall", homeroomStart + 10, this.hallMinutes));
    }
    var letterIndex = blockLetters.indexOf(this.dayLetter), letter, userData;
    for (var i = 0; i < this.periodsLength; i++) {
      letter = blockLetters[letterIndex];
      lastBlock = this.blocks[this.blocks.length - 1];
      if (User.loggedin) {
        userData = User.classes[letter];
      }
      if (i === this.lunchPeriod) {
        if (dom.lunchBlock.textContent === "1") {
          this.blocks.push(new Block(letter + " - First lunch", lastBlock.end + 5, 25, userData));
          this.blocks.push(new Block(letter,  lastBlock.end + 30, 45, userData)); // the passing period
        } else if (dom.lunchBlock.textContent === "2") {
          this.blocks.push(new Block(letter, lastBlock.end + 5, 45, userData));
          this.blocks.push(new Block(letter + " - Second Lunch",  lastBlock.end + 50, 25, userData));
        } else {
          this.blocks.push(new Block(letter + " - First Lunch", lastBlock.end + 5, 25, userData));
          this.blocks.push(new Block(letter + " - Passing Period",  lastBlock.end + 30, 20, userData)); // the passing period
          this.blocks.push(new Block(letter + " - Second Lunch",  lastBlock.end + 50, 25, userData));
        }
      } else {
        this.blocks.push(new Block(letter, lastBlock.end + 5, this.periodDuration, userData));
      }
      letterIndex = (letterIndex + 1) % 8;
    }
  }

  Day.prototype.domElement = function() {
    var element = document.createElement("div"), child;
    element.className = "list-section";
    child = document.createElement("h3");
    if (this.noClasses) {
      child.textContent = this.dayString;
      element.appendChild(child);
      return element;
    }
    child.textContent = this.dayString;
    element.appendChild(child);
    if (this.special) {
      return element;
    }
    for (var i = 0; i < this.blocks.length; i++) {
      element.appendChild(this.blocks[i].domElement());
    }
    return element;
  };
  
  Day.BEFORE_SCHOOL = -1;
  Day.AFTER_SCHOOL = 20;
  Day.HOMEROOM = -2;
  Day.HALL = -1;
  
  if (User.loggedin) {
    User.setClassesFrom(dom.classes);
  }
  
  isDrop = document.getElementById("dropday").textContent === "true";
  dom.dayType.textContent = "H";
  today = new Day(dom.dayType.textContent, isDrop, (User.loggedin ? User.classes : undefined));
  days.push(today);
  
  dom.currentDay = today.domElement();
  dom.periodsList.appendChild(dom.currentDay);
  dom.lunch.style.display = "none";
  function onminutechange() {
    now.setTime(Date.now());
    // find a block which starts after the current time (in minutes)
    fullMinutes = now.getMinutes() + now.getHours() * 60;
    
    if (fullMinutes < today.blocks[0].start) { // before homeroom
      currentBlock = Day.BEFORE_SCHOOL;
      if (oldBlock !== currentBlock) {
        dom.currentBlock.textContent = "Before homeroom";
        dom.minutesLeft.textContent = "Good morning!";
        oldBlock = currentBlock;
      }
    } else if (fullMinutes >= today.blocks[today.blocks.length - 1].end) { // after last block
      currentBlock = Day.AFTER_SCHOOL;
      if (oldBlock !== currentBlock) {
        if (today.blocks[oldBlock]) {
          today.blocks[oldBlock].element.style.backgroundColor = "";
        }
        dom.currentBlock.textContent = "After school";
        dom.minutesLeft.textContent = "Good night!";
        oldBlock = currentBlock;
      }
    } else { // during school
      while (currentBlock < today.blocks.length && today.blocks[currentBlock].end + 5 <= fullMinutes) {
        currentBlock++;
      }
      if (currentBlock !== oldBlock) {
        if (today.blocks[oldBlock]) {
          today.blocks[oldBlock].element.style.backgroundColor = "";
        }
        today.blocks[currentBlock].element.style.backgroundColor = "#a3ba82";
        dom.currentBlock.textContent = today.blocks[currentBlock].text();
        oldBlock = currentBlock;
      }
      minutesLeft = today.blocks[currentBlock].end - fullMinutes; // minutes remaining until end of block
      if (minutesLeft < 1) {
        dom.minutesLeft.textContent = "Passing time: " + (minutesLeft + 5) + " minute" + (minutesLeft + 5 === 1 ? "" : "s") + " until " + today.blocks[currentBlock + 1].text() + " begins";
        if (minutesLeft === 0) {
          timeLeftAlarm();
        }
      } else {
        dom.minutesLeft.textContent = minutesLeft + " minute" + (minutesLeft === 1 ? "" : "s") + " left in this block";
        if (minutesLeft === 5) {
          timeLeftAlarm("5 minutes left in this block");
        }
      }
      if (dom.currentBlock.textContent.indexOf("lunch") !== -1) {
        dom.lunch.style.display = "";
      } else {
        dom.lunch.style.display = "none";
      }
    }
  }
  
  function timeLeftAlarm(text) {
    if (User.Settings.notifications) {
      notifyWith("RL Schedule", text || dom.minutesLeft.textContent);
    }
    if (User.Settings.audio) {
      User.Settings.audioSound.play();
    }
  }
  
  if (today.noClasses) {
    dom.currentBlock.textContent = today.dayString;
    dom.minutesLeft.textContent = "There is no school today.";
  } else if (today.special) {
    dom.currentBlock.textContent = "Special schedule";
    dom.minutesLeft.textContent = "Please use the schedule sheet given at school.";
  } else {
    onminutechange();
    setTimeout(function() {
      onminutechange();
      setInterval(onminutechange, 60000);
    }, 60000 - now.getSeconds() * 1000);
  }
  
  /* --- settings --- */
  User.Settings = {
    notifications: false,
    audio: false
  };
  
  User.initSettings = function() {
    dom.allSettings = dom.settings.getElementsByTagName("input");
    
    if (this.loggedin) {
      // get settings from server...
    } else if (window.localStorage) {
      this.Settings = JSON.parse(localStorage.getItem("settings")) || this.Settings;
    }
    
    if (this.Settings.audio) {
      this.Settings.audioSound = document.createElement("audio");
      this.Settings.audioSound.src = "chime.mp3";
    }
    var currentSetting;
    for (var i = 0; i < dom.allSettings.length; i++) {
      currentSetting = dom.allSettings[i];
      currentSetting.checked = this.Settings[currentSetting.getAttribute("data-rep")];
    }
    
    dom.settingsButton.addEventListener("click", function() {
      dom.settings.style.display = "block";
      dom.settings.scrollIntoView({block: "start", behavior: "smooth"});
    });
    
    dom.settingsSubmit.addEventListener("click", function() {
      User.saveSettings();
    
      dom.settings.style.display = "none"; // hide settings
      document.body.scrollTop = 0; // return to top of page
    });
  };
  
  User.saveSettings = function() {
    var currentSetting;
    for (var i = 0; i < dom.allSettings.length; i++) { // get settings from checkboxes
      currentSetting = dom.allSettings[i];
      this.Settings[currentSetting.getAttribute("data-rep")] = currentSetting.checked;
    }
    if (this.loggedin) {
      ajax({
        url: "http://rlclock.tk/settings",
        type: "POST",
        data: {
          user: document.cookie.replace("user=",""),
          notifications: this.Settings.notifications,
          audio: this.Settings.audio
        },
        success: function(xmlhttp) {
          alert("data saved successfully");
        },
        error: function(err) {
          alert(err);
        }
      });
    } else {
      localStorage.setItem("settings", JSON.stringify(this.Settings));
    }
  };
  
  User.initSettings();
  

  function notifyWith(title, body) {
    if (!window.Notification) {
      return alert("Notifications not supported."); // use better error message
    } else if (window.Notification.permission === "granted") {
      display();
    } else if (window.Notification.permission !== "denied") {
      window.Notification.requestPermission(function(permission) {
        if (permission === "granted") {
          display();
        }
      });
    }
    function display() {
      if (lastNotification) {
        setTimeout(lastNotification.close.bind(lastNotification), 5000);
      }
      lastNotification = new window.Notification(body, {title: title});
    }
  }
  
  /* --- mobile support --- */
  dom.nav.button.addEventListener("touchstart", function() {
    dom.nav.el.classList.toggle("mobileHover");
  });
  
  /* --- log in, sign up, log out interface --- */
  var LocalNotification = { // object representing success/failure notification.
    el: document.getElementById("local-notification"),
    show: function() {
      this.el.classList.add("notification-show");
      return this;
    },
    hide: function() {
      this.el.classList.remove("notification-show");
      return this;
    },
    text: function(text) {
      this.el.textContent = text;
      return this;
    }
  };
  if (LocalNotification.el.textContent !== "") {
    LocalNotification.show();
  }
  
  document.getElementById("logout").addEventListener("click", function() {
    document.cookie="user=";
    //LocalNotification.text("logged out").show();
    window.location.href = "http://rlclock.tk"; // temporary.
  });
  
  /* --- ajax helper function --- */
  function ajax(info) {
    var request = new XMLHttpRequest();
    
    request.addEventListener("load", function() {
      if (request.status >= 200 && request.status < 400) {
        info.success(this);
      } else {
        info.error("server returned error");
      }
    });
    request.addEventListener("error", function() {
      info.error("connection error");
    });
    
    request.open(info.type, info.url, true);
    if (info.type === "POST") {
      request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      request.send(info.data);
    } else {
      request.send();
    }
  }
})();
