var mongoose = require("mongoose");
var userSchema = mongoose.Schema({
    _id: String,
    a: {
        name: String,
        lunch: Number
    },
    b: {
        name: String,
        lunch: Number
    },
    c: {
        name: String,
        lunch: Number
    },
    d: {
        name: String,
        lunch: Number
    },
    e: {
        name: String,
        lunch: Number
    },
    f: {
        name: String,
        lunch: Number
    },
    g: {
        name: String,
        lunch: Number
    },
    h: {
        name: String,
        lunch: Number
    },
    notifications: Boolean,
    audio: Boolean
});
var User = mongoose.model("User", userSchema);
module.exports = User;
