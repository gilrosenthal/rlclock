/* Jon: Filled in the next 2 weeks as of September 27. When will we be streamed the data? */

var exp = {};
var days = [];
days["Mon Sep 28 2015"] = "F-20";
days["Tue Sep 29 2015"] = "E";
days["Wed Sep 30 2015"] = "D Drop";
days["Thu Oct 1 2015"] = "B";
days["Fri Oct 2 2015"] = "A";

days["Mon Oct 5 2015"] = "E";
days["Tue Oct 6 2015"] = "D-S";
days["Wed Oct 07 2015"] = "C Drop";
days["Thu Oct 08 2015"] = "A-35";
days["Fri Oct 09 2015"] = "H Drop";

var lunch = [];
lunch["Mon Sep 28 2015"] = "rigatoni, bbq grilled chicken";
lunch["Tue Sep 29 2015"] = "cheesesteak and buffalo chicken pasta";
lunch["Wed Sep 30 2015"] = "chicken sandwich";
lunch["Thu Oct 1 2015"] = "tacos";
lunch["Fri Oct 2 2015"] = "cod, beef burgundy";

lunch["Mon Oct 5 2015"] = "chicken marsala";
lunch["Tue Oct 6 2015"] = "popcorn chicken";
lunch["Wed Oct 7 2015"] = "pizza";
lunch["Thu Oct 8 2015"] = "sloppy joes";
lunch["Fri Oct 9 2015"] = "fish sandwich";

exp.days = days;
exp.lunch = lunch;
module.exports=exp;
