var exp = {};
exp.getLunchBlock = function(day){
  if(!day){
    return 1;
  }
var blockLetters = "ABCDEFGH", currentBlock = 0, fullMinutes, homeroomStart = 495, isDrop, minutesLeft, oldBlock, today;
var days = [], User;
function Block(letter, start, length, name) {
    this.letter = letter;
    if (name) {
      this.name = name;
    }
    this.start = start;
    this.end = start + length;
    this.length = length;
  }
  
  Block.prototype.text = function() {
    return this.name ? this.letter + ": " + this.name : this.letter;
  };
  

  Block.minutesToString = function(mins) {
    var h = (Math.floor(mins / 60) % 12 || 12).toString();
    var m = (mins % 60).toString();
    return h + ":" + (m.length === 1 ? "0" + m : m);
  };

  function Day(dayStr, isDrop) {
    this.dayString = dayStr;
    if (this.dayString === "0") { // no school, no need to do anything else
      return this;
    }
    var splitDay = dayStr.split("-");
    if (splitDay[1] === "S") { // special schedule, again, no need
      this.special = true;
      return this;
    }
    this.dayLetter = splitDay[0];
    this.hallMinutes = 0;
    if (splitDay.length > 1) {
      this.hallMinutes = parseInt(splitDay[1], 10);
    }

    this.periodDuration = 45;
    if (this.hallMinutes > 30) {
      if (this.hallMinutes === 45) {
        this.periodDuration = 44;
      } else if (this.hallMinutes === 60) {
        this.periodDuration = 42;
      } else if (this.hallMinutes === 70) {
        this.periodDuration = 40;
      }
    }
    this.isDrop = isDrop;
    this.periodsLength = 7 - this.isDrop;
    this.lunchPeriod = this.hallMinutes >= 45 ? 2 : 3;

    this.blocks = [];
    var lastBlock;
    this.blocks.push(new Block("Homeroom", homeroomStart, 5));
    if (this.hallMinutes) {
      this.blocks.push();
    }
    var letterIndex = blockLetters.indexOf(this.dayLetter), userData;
    for (var i = 0; i < this.periodsLength; i++) {
      lastBlock = this.blocks[this.blocks.length - 1];
      if (User) {
        userData = User.data[blockLetters[letterIndex]];
      }
      if (i === this.lunchPeriod) {
        this.blocks.push(new Block(blockLetters[letterIndex] + " - First lunch", lastBlock.end + 5, 25, userData));
        this.blocks.push(new Block(blockLetters[letterIndex] + " - Between lunches", lastBlock.end + 30, 20, userData)); // the passing period
        this.blocks.push(new Block(blockLetters[letterIndex] + " - Second lunch", lastBlock.end + 50, 25, userData));
      } else {
        this.blocks.push(new Block(blockLetters[letterIndex], lastBlock.end + 5, this.periodDuration, userData));
      }
      letterIndex = (letterIndex + 1) % 8;
    }
  }

  
  Day.BEFORE_SCHOOL = -1;
  Day.AFTER_SCHOOL = 20;
  Day.HOMEROOM = -2;
  Day.HALL = -1;
  
 
  
  days.push(new Day(day, false, (User ? User.data : undefined)));
  return days[0].blocks[days[0].lunchPeriod].letter.replace(" - First lunch","").trim().replace(" ","").trim();
};
module.exports = exp;