var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var daySchema = new Schema({
    _id : String,
    type: String,
    news:[{sender:String,
        message:String
    }]
});
var Day = mongoose.model("Day",daySchema);
module.exports = Day;
